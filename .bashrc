if [[ -r $XDG_CONFIG_HOME/aliases ]]; then
    source $XDG_CONFIG_HOME/aliases
fi

mkdir -p $XDG_CACHE_HOME/bash

PS1='\e[31m\u\e[33m@\e[32m\h\e[33m:\e[36m$PWD\e[0m\n% '

if [[ ! -z "${VIRTUAL_ENV}" ]]; then
    prefix=$(basename "${VIRTUAL_ENV}")
    PS1="($prefix) $PS1"
fi

HISTCONTROL=ignoreboth:erasedups
HISTFILE=$XDG_CACHE_HOME/bash/history
HISTSIZE=1000
HISTFILESIZE=2000
INPUTRC=$XDG_CONFIG_HOME/readline/inputrc

shopt -s checkjobs
shopt -s checkwinsize
shopt -s cmdhist
shopt -s globstar
shopt -s histappend
shopt -s lithist
shopt -s no_empty_cmd_completion

set -o notify
set -o noclobber
set -o vi

#BEGIN mchp
#export DISPLAY=localhost:0.0
#PROMPT_COMMAND='echo -en "\033]0;$(\dirs +0)\a"'
#END mchp
