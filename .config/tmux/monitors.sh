#!/bin/sh

if command -v htop >/dev/null 2>&1; then
    tmux new-window -a -n monitors htop
    tmux split-window -t monitors.0 -h iostat -c 5
fi
if command -v sensors -ng >/dev/null 2>&1; then
    tmux split-window -t monitors.1 -v watch --no-title --interval 5 sensors
    tmux resize-pane -t monitors.1 -x 80 -y 5
fi
if command -v bwm-ng >/dev/null 2>&1; then
    tmux split-window -t monitors.2 -v bwm-ng -t 2000 --dynamic --output curses
    tmux resize-pane -t monitors.2 -y 15
fi
