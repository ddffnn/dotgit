#!/usr/bin/env bash

if ! command -v jq >/dev/null 2>&1; then
    i3-nagbar  -m "command jq not found" &
    exit 1
fi

if [ "$1" = "dmenu" ]; then
    WORKSPACES=($(i3-msg -t get_workspaces | jq -r '.[] | .name' | sort --version-sort))
    IFS=':' read -ra CHOICE < <(printf '%s\n' "${WORKSPACES[@]}" | dmenu $DMENU_ARGS -b)
    WORKSPACE=${CHOICE[0]}
else
    WORKSPACE=$1
fi

if [ -z $WORKSPACE ]; then
    exit 2
fi

set -x
OUTPUT=$(i3-msg -t get_workspaces | jq -r '.[] | select(.focused) | .output')
CURRENT=$(i3-msg -t get_outputs | jq -r ".[] | select(.current_workspace==\"$WORKSPACE\") | .name")
if [ "$OUTPUT" == "$CURRENT" ]; then
    exit 0
fi
i3-msg --quiet workspace number $WORKSPACE
i3-msg --quiet move workspace to output $OUTPUT
i3-msg --quiet workspace number $WORKSPACE

# vim: set ft=bash
