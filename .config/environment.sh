#!/usr/bin/env sh

if [ -z $XDG_CONFIG_HOME ]; then
    export XDG_CONFIG_HOME=$HOME/.config
fi
if [ -z $XDG_CACHE_HOME ]; then
    export XDG_CACHE_HOME=$HOME/.cache
fi
if [ -z $XDG_DATA_HOME ]; then
    export XDG_DATA_HOME=$HOME/.local/share
fi

if [ -r $XDG_CONFIG_HOME/pathrc ]; then
    . $XDG_CONFIG_HOME/pathrc
fi

if [ -r $XDG_CONFIG_HOME/secrets.sh ]; then
    source $XDG_CONFIG_HOME/secrets.sh
fi

if [ -d $XDG_CONFIG_HOME/zsh ]; then
    export ZDOTDIR=$XDG_CONFIG_HOME/zsh
fi

MANPATH=$HOME/share/man
MANPATH=$MANPATH:/usr/share/man
MANPATH=$MANPATH:/usr/local/share/man
export MANPATH

LD_LIBRARY_PATH=$HOME/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-redhat-linux5E/lib64 # ???
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/java/default/jre/lib/amd64/server # ???
export LD_LIBRARY_PATH

PKG_CONFIG_PATH=$HOME/lib/pkgconfig
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib64/pkgconfig # centos 6
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/share/pkgconfig # centos 6
export PKG_CONFIG_PATH

# These screwed up mcuxpresso by screwing up arm gcc
# CPATH=$HOME/include
# CPATH=$CPATH:/usr/include
# export CPATH

export EDITOR=vim
export PAGER=less
export LESS='--RAW-CONTROL-CHARS --ignore-case --jump-target=5 --search-skip-screen'
export LESSHISTFILE=$XDG_CACHE_HOME/lesshistfile
#export PYTHONPATH=$HOME/lib/python
export TERMINFO=$XDG_DATA_HOME/terminfo
export UNISON="$XDG_CONFIG_HOME"/unison

if [ -r $XDG_CONFIG_HOME/vim/vimrc ]; then
    export VIMINIT=":source $XDG_CONFIG_HOME/vim/vimrc"
fi
if [ ! -z $XDG_CACHE_HOME ]; then
    mkdir -p "$XDG_CACHE_HOME"/vim/{undo,swap,backup,view}
fi

if [ -d $XDG_CONFIG_HOME/gnupg ]; then
    export GNUPGHOME=$XDG_CONFIG_HOME/gnupg
fi

# ruby
command -v rbenv >/dev/null 2>&1 && eval "$(rbenv init -)"

# R
if [ -r $XDG_CONFIG_HOME/r/Renviron ]; then
    export R_ENVIRON_USER=$XDG_CONFIG_HOME/r/Renviron
fi
if [ -r $XDG_CONFIG_HOME/r/Rprofile ]; then
    export R_PROFILE_USER=$XDG_CONFIG_HOME/r/Rprofile
fi
export R_HISTFILE=$XDG_CACHE_HOME/r/history

H=$(hostname)
case "$H" in
    daffy)
        export TMPDIR=/local/ddffnn/tmp
        export TEMP=/local/ddffnn/tmp
        export TMP=/local/ddffnn/tmp
        ;;
    *)
        ;;
esac

if [ "$OSTYPE" = "msys" ]; then
   if grep --silent enable-putty-support $HOME/scoop/apps/gnupg/current/home/gpg-agent.conf
   then
       eval `ssh-pageant -S bourne --reuse -a "/tmp/ssh-pageant-$USERNAME"`
   fi
fi

# nvm
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# acroname
export ACRONAME_PROJECTS=$HOME/projects
