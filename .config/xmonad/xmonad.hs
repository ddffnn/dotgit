import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Util.Loggers
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig
import qualified XMonad.StackSet as W
import System.IO
import XMonad.Actions.PhysicalScreens
import XMonad.Hooks.EwmhDesktops
import Data.List (isInfixOf)

-- layouts
import XMonad.Layout.Tabbed
import XMonad.Layout.Renamed
import XMonad.Layout.SimplestFloat
import XMonad.Layout.NoBorders
import XMonad.Layout.Reflect
import XMonad.Layout.Column
import XMonad.Layout.MultiColumns

main = do
    statuspipe <- spawnPipe myStatusBar
    spawn "stalonetray --config $XDG_CONFIG_HOME/stalonetray/stalonetrayrc"
    xmonad $ ewmh def {
        handleEventHook = fullscreenEventHook <+> docksEventHook,
        terminal   = "alacritty",
        workspaces = ["1","2","3","4","5","6","7", "8", "9"],
        manageHook = myManageHook <+> manageDocks <+> manageHook def,
        layoutHook = myLayoutHook,
        logHook    = dynamicLogWithPP $ def {
            ppCurrent         = dzenColor "red" "",
            ppVisible         = dzenColor "gray90" "",
            ppHidden          = dzenColor "grey50" "",
            ppHiddenNoWindows = dzenColor "grey10" "",
            ppUrgent          = dzenColor "yellow" "",
            ppLayout          = dzenColor "red" "",
            ppSep             = " ^fg(yellow)|^fg(white) ",
            ppOutput          = hPutStrLn statuspipe,
            ppExtras          = myPpExtras,
            ppOrder           = \(ws:l:t:xs) -> [ws,l] ++ xs
        }
    } `additionalKeysP` myKeys `removeKeysP` [ "M-m" ]

myLayoutHook = avoidStruts ( threeCol ||| twoCol ||| oneCol ||| tabs ||| float ||| browse ) ||| full
    where
        oneCol   = renamed [Replace "|=====|"] $ Column 1.0
        twoCol   = renamed [Replace "|==|  |"] $ reflectHoriz       $ Tall 1 (3/100) (1/2)
        threeCol = renamed [Replace "|=|-| |"] $ reflectHoriz       $ multiCol [1,2,0] 0 (3/100) (-1/3)
        browse   = renamed [Replace "|_____|"] $ reflectVert        $ Column 0.2
        tabs     = renamed [Replace "|_,_,_|"] $ noBorders simpleTabbedBottom
        float    = renamed [Replace "[] ▬▪ ■"] $ simplestFloat
        full     = renamed [Replace "[     ]"] $ noBorders Full

myManageHook = composeAll $ 
    [
        className =? "Xdialog" --> doCenterFloat,
        className =? "TeamViewer" --> doCenterFloat,
        className =? "Firefox" --> doCenterFloat,
        className =? "qpdfview" --> doShift "5",
        fmap (isInfixOf "ImageMagick:") title --> doCenterFloat,
        isFullscreen --> doFullFloat
    ]

myKeys = 
    [
        ("M-,", viewScreen 0) ,
        ("M-.", viewScreen 1) ,
        ("M-p", spawn ("dmenu_run -fn '" ++ myFont ++"'")) ,
        ("M-c", spawn ("yubikey-oath-dmenu --clipboard clipboard"))
    ]

myStatusBar = "dzen2 -x 0 -w 1600 -ta l -fn '" ++ myFont ++ "' -fg white -bg black"
myFont = "xos4 Terminus:size=9"
myPpExtras = [loadAvg, date "%a %b %-d", dzenColorL "red" "black" (date "%R"), logTitle]
