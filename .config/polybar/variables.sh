#!/usr/env/bin bash

# mpd settings from $XDG_CONFIG_HOME/secrets.sh sourced in environment.sh

H=$(hostname -s)
case "$H" in
    20GL0007US)
        export PRIMARY_RIGHT="fastmail iastate trackpad touchscreen xkeyboard pulseaudio battery iowait load temperature date"
        export NETWORKING_RIGHT="wlan wlanrate ip"
        export LAN_INT=enp0s3
        export WLAN_INT=wlan0
        export PWR_ADAPTER=ADP1
        ;;
    nucinfutz)
        export PRIMARY_RIGHT="xkeyboard pulseaudio iowait load temperature date"
        export NETWORKING_RIGHT="lan wlan ip"
        export LAN_INT=eno1
        export WLAN_INT=wlp2s0
        export THERMAL_ZONE=2
        ;;
    tdh-ubuntu)
        export PRIMARY_RIGHT="zoho xkeyboard backlight trackpad pulseaudio battery iowait load temperature date"
        export NETWORKING_RIGHT="bluetooth lan wlan ip"
        #export LAN_INT=enp0s31f6 # mobo ethernet
        #export LAN_INT=enx00249b2b5d40 #
        export LAN_INT=enxa0cec817e171 # monoprice white 15250
        export WLAN_INT=wlp0s20f3
        export PWR_ADAPTER=AC
        export THERMAL_ZONE=0
        export BTID=50:C2:ED:BF:74:D9
        ;;
esac

export UPDATE_CLICK_COMMAND_1='kill -USR1 $(pgrep --oldest --parent %pid%)'
export UPDATE_CLICK_COMMAND_2='kill -USR2 $(pgrep --oldest --parent %pid%)'
