if [[ -r $ZDOTDIR/zshcomp ]]; then
    source $ZDOTDIR/zshcomp
fi

if [[ -r $ZDOTDIR/zgen.zsh ]]; then
    export ZGEN_DIR=$ZDOTDIR/zgen
    export ZGEN_INIT=$ZGEN_DIR/init.zsh
    export ZGEN_AUTOLOAD_COMPINIT=false
    source $ZDOTDIR/zgen.zsh
    function zgload(){
        unset ZGEN_LOADED
        zgen load zsh-users/zsh-syntax-highlighting
        zgen load voronkovich/gitignore.plugin.zsh
        zgen save
    }
    function zgsu() {
        curl -L 'https://github.com/tarjoilija/zgen/raw/master/zgen.zsh' -o $ZDOTDIR/zgen.zsh
        if [ $? -eq 0 ]; then
            source $ZDOTDIR/zgen.zsh
            zgen reset
            zgload
        fi
    }
fi

if [[ -r $XDG_CONFIG_HOME/aliases ]]; then
    source $XDG_CONFIG_HOME/aliases
fi

if [[ -r $XDG_CONFIG_HOME/dircolors ]]; then
    eval $(dircolors $XDG_CONFIG_HOME/dircolors)
fi

if [[ -r $XDG_CONFIG_HOME/less_termcap ]]; then
    source $XDG_CONFIG_HOME/less_termcap
fi

#BEGIN mchpcad
## modules stuff for cad machines
#module() { eval `/usr/bin/modulecmd zsh $*`; }
#export MODULESHOME=/home/users/cad/modules
#if [ "${MODULEPATH:-}" = "" ]; then
#  export MODULEPATH=`sed -n 's/\s*#.*$//;/./H;${x;s/^\n//;s/\n/:/g;p}' ${MODULESHOME}/init/.modulespath`
#fi
#if [[ -r $HOME/.modules ]]; then
#    source $HOME/.modules
#fi
#END mchpcad

if [[ -r $XDG_CONFIG_HOME/readline/inputrc ]]; then
    export INPUTRC=$XDG_CONFIG_HOME/readline/inputrc
fi

export GREP_COLOR="38;5;202"

# misc options {
setopt EXTENDED_GLOB # use # ~ and ^ in filename generation
unsetopt NOTIFY # so that notification of bg job status comes with new prompt
setopt PRINT_EXIT_VALUE # print return value iff return value != 0
#}

# zle {
bindkey -r '^[/'    # get rid of screwy binding for full editor
bindkey -v            # vi line editing
# allow jumping back to previos history entries with same beginning
bindkey "^R" history-beginning-search-backward
bindkey "^F" history-beginning-search-forward
# next 2 lines emulate vim and allow to backspace before insertion point
bindkey "^W" backward-kill-word    # vi-backward-kill-word
bindkey "^?" backward-delete-char  # vi-backward-delete-char
autoload edit-command-line      # \
zle -N edit-command-line        #  |-> These lines bind v to full editor
bindkey -a v edit-command-line  # /
# }

# shell parameters {
LISTMAX=0            # list as many completions as will fit in terminal
MAILCHECK=0            # disable mail checking
# }

# cd options {
setopt AUTO_PUSHD        # add previous directory to directory stack
setopt CHASE_LINKS        # resolve links in cd operations
setopt PUSHD_IGNORE_DUPS    # disallow duplicates in stack
DIRSTACKSIZE=5          # max size of directory stack
# }

# completion {
# the default behavior is quite nice, but much can be tweaked
setopt COMPLETE_IN_WORD        # allow tab completion in the middle of a word
setopt LIST_PACKED        # compress the completion list by adjusting col width
setopt LIST_AMBIGUOUS       # wait until 2nd tab press to show completion list
ZLE_SPACE_SUFFIX_CHARS=$'&|'
ZLE_REMOVE_SUFFIX_CHARS=$' \n;&' # don't eat space before | after tab completion (remove \t from default list)
# }

# command history tweaks {
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=$XDG_DATA_HOME/zsh/history
setopt HIST_IGNORE_ALL_DUPS    # remove old duplicates from history
setopt HIST_REDUCE_BLANKS    # trim excess blanks
setopt SHARE_HISTORY        # import commands for hist file
# }

# i/o options {
setopt NO_CLOBBER        # require >| to overwrite; >>| to create
setopt CORRECT        # spelling correction
setopt DVORAK            # use dvorak kb as basis for spelling corrections
# }

# job control options {
setopt NO_BG_NICE        # keep background processes at full speed
setopt CHECK_JOBS        # report on bg jobs at shell exit
setopt HUP                # hang up jobs on shell exit
# }

# show long exec times {
export REPORTTIME=5
export TIMEFMT='%*E %MMiB'
# }

# PROMPT {
# remove space at end of prompt
# https://superuser.com/a/726509
ZLE_RPROMPT_INDENT=0
autoload colors
colors
PS1=$'%{\e[31m%}%#%{\e[0m%} '
RPS1=$'%{\e[31m%}%n%{\e[33m%}@%{\e[32m%}%m%{\e[33m%}:%{\e[36m%}%d%{\e[33m%}'
PS2=$'%{\e[31m%}%_>%{\e[0;33m%} '
zle-line-init () {
    echo -ne "\e[33m"
};
zle -N zle-line-init;
zle-line-finish () {
 echo -ne "\e[0m"
};
zle -N zle-line-finish
# make prompt update every second so time of execution in displayed
# https://stackoverflow.com/a/17915194
# quit using it for RAM usage reasons
# TMOUT=1
# TRAPALRM() {
#     if [ "$WIDGET" != "complete-word" ]; then
#         zle reset-prompt
#     fi
# }
# }

# git stuff {
__git_files () {
    _wanted files expl 'local files' _files  }
# }

# change cursor based on mode {
# https://unix.stackexchange.com/a/344028
zle-keymap-select () {
    if [ $KEYMAP = vicmd ]; then
        printf "\033[2 q" # block
    else
        printf "\033[6 q" # line
    fi
}
zle -N zle-keymap-select
    zle-line-init () {
    zle -K viins
    printf "\033[6 q"
}
zle -N zle-line-init
preexec(){
    echo -ne "\e[0m"
    printf "\033[2 q"
}
# }

# atuin
if command -v atuin >/dev/null 2>&1; then
    # https://github.com/ellie/atuin/blob/main/docs/key-binding.md
    eval "$(atuin init --disable-up-arrow zsh)"
fi

# keep this at the end of .zshrc
# https://superuser.com/a/230090
if [[ $1 == eval ]]
then
    "$@"
    set --
fi

# pyenv
if command -v pyenv >/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi
