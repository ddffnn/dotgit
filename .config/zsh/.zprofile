if [[ -r $XDG_CONFIG_HOME/environment.sh ]]; then
    source $XDG_CONFIG_HOME/environment.sh
else
    if [[ -r $HOME/.config/environment.sh ]]; then
        source $HOME/.config/environment.sh
    fi
fi

# vim: set filetype=zsh

