# initially sourced from github.com/jtheoof/dotfiles

# COLOR needs one of these arguments: 'tty' colorizes output to ttys, but not
# pipes. 'all' adds color characters to all output. 'none' shuts colorization
# off.
COLOR tty

# Below, there should be one TERM entry for each termtype that is colorizable
TERM ansi
TERM color_xterm
TERM color-xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM cons25
TERM console
TERM cygwin
TERM dtterm
TERM Eterm
TERM eterm-color
TERM gnome
TERM gnome-256color
TERM jfbterm
TERM konsole
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM mlterm
TERM nxterm
TERM putty
TERM rxvt
TERM rxvt-256color
TERM rxvt-cygwin
TERM rxvt-cygwin-native
TERM rxvt-unicode
TERM rxvt-unicode256
TERM rxvt-unicode-256color
TERM screen
TERM screen-256color
TERM screen-256color-bce
TERM screen-bce
TERM screen.linux
TERM screen-w
TERM tmux
TERM vt100
TERM xterm
TERM xterm-16color
TERM xterm-256color
TERM xterm-88color
TERM xterm-color
TERM xterm-debian
TERM xterm-termite

# EIGHTBIT, followed by '1' for on, '0' for off. (8-bit output)
EIGHTBIT 1

# code : attr
# ---- : ----
# 00   : none
# 01   : bold
# 04   : underscore
# 05   : blink
# 07   : reverse
# 08   : concealed

# fore : back : color
# code : code : name
# ---- : ---- : ----
# 30   : 40   : black
# 31   : 41   : red
# 32   : 42   : green
# 33   : 43   : yellow
# 34   : 44   : blue
# 35   : 45   : magenta
# 36   : 46   : cyan
# 37   : 47   : white

# for extended colors
# 38;5;n where n is color index
# 38;2;r;g,b for rgb specification

NORMAL 1;37 # default
FILE   1;37 # file
DIR    1;35 # directory

LINK        4;1;37 # symlink
ORPHAN  7;38;5;226 # bad symlink
MISSING 7;38;5;226 # bad symlink

FIFO 33;47 # fifo
SOCK 33;47 # socket
DOOR 33;47 # Solaris 2.5 and later
BLK  33;47 # block
CHR  33;47 # character

# dir that is sticky and other-writable (+t,o+w)
STICKY_OTHER_WRITABLE 1;33;41
# BEGIN wsl
#STICKY_OTHER_WRITABLE 1;35
# END wsl

# dir that is other-writable (o+w) and not sticky
OTHER_WRITABLE 1;33;41
# BEGIN wsl
#OTHER_WRITABLE 1;35
# END wsl

# temp files
#
.swp 38;5;238
.aux 38;5;238
*~    38;5;238

# files with execute permission or binaries
EXEC 38;5;196 # Unix
.exe 38;5;196 # Win
.app 38;5;196 # OSX

# scripts
.awk 38;5;202
.sed 38;5;202
.mk  38;5;202
.pl  38;5;202
.sh  38;5;202
.gp  38;5;202
.r   38;5;202
.R   38;5;202
.py  38;5;202
.zsh 38;5;202
.cmd 38;5;202 # Win
.com 38;5;202 # Win
.bat 38;5;202 # Win
.reg 38;5;202 # Win

# Source
#
.C     38;5;220
.c     38;5;220
.cc    38;5;220
.csh   38;5;220
.css   38;5;220
.cxx   38;5;220
.el    38;5;220
.h     38;5;220
.hs    38;5;220
.java  38;5;220
.man   38;5;220
.objc  38;5;220
.php   38;5;220
.pm    38;5;220
.pod   38;5;220
.rb    38;5;220
.rdf   38;5;220
.vim   38;5;220
.xml   38;5;220

# plain text
#
.txt  1;33
.csv  1;33
.tsv  1;33
.json 1;33
.xml  1;33

# markup
#
.md   1;34
.mkd  1;34
.tex  1;34
.html 1;34
.htm  1;34
.js   1;34

# redered documents
#
.pdf  1;36
.ps   1;36
.dvi  1;36
.mobi 1;36
.djvu 1;36
.azw  1;36
.epub 1;36

# binary
#
.vi   32
.seq  32
.bin  32
.seq  32
.skp  32
.doc  32
.docx 32
.rtf  32
.dot  32
.dotx 32
.xls  32
.xlsx 32
.ppt  32
.pptx 32
.fla  32
.psd  32

# Image
#
.bmp  36
.cgm  36
.dl   36
.emf  36
.eps  36
.gif  36
.jpeg 36
.jpg  36
.JPG  36
.mng  36
.pbm  36
.pcx  36
.pgm  36
.png  36
.ppm  36
.pps  36
.ppsx 36
.svg  36
.svgz 36
.tga  36
.tif  36
.tiff 36
.xbm  36
.xcf  36
.xpm  36
.xwd  36
.xwd  36
.yuv  36

# Audio
#
.aac  36
.au   36
.flac 36
.mid  36
.midi 36
.mka  36
.mp3  36
.mpa  36
.mpeg 36
.mpg  36
.ogg  36
.ra   36
.wav  36

# Video
#
.anx  36
.asf  36
.avi  36
.axv  36
.flc  36
.fli  36
.flv  36
.gl   36
.m2v  36
.m4v  36
.mkv  36
.mov  36
.mp4  36
.mp4v 36
.mpeg 36
.mpg  36
.nuv  36
.ogm  36
.ogv  36
.ogx  36
.qt   36
.rm   36
.rmvb 36
.swf  36
.vob  36
.wmv  36

# Archives
#
.7z   35
.apk  35
.arj  35
.bin  35
.bz   35
.bz2  35
.cab  35 # Win
.deb  35
.dmg  35 # OSX
.gem  35
.gz   35
.iso  35
.jar  35
.msi  35 # Win
.rar  35
.rpm  35
.tar  35
.tbz  35
.tbz2 35
.tgz  35
.tx   35
.war  35
.xpi  35
.xz   35
.z    35
.Z    35
.zip  35

