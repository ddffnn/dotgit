""# set nocompatible " this needs to be early because it resets other things

" https://tlvince.com/vim-respect-xdg
" viminfofile used instead of viminfo though
"" set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after
""# set undodir=$XDG_CACHE_HOME/vim/undo
""# set directory=$XDG_CACHE_HOME/vim/swap
""# set backupdir=$XDG_CACHE_HOME/vim/backup
"" set viewdir=$XDG_CACHE_HOME/vim/view
"" set viminfofile=$XDG_CACHE_HOME/vim/viminfo
"" set spellfile=$XDG_CACHE_HOME/vim/spellfile.utf-8.add

" mkdir -p "$XDG_CACHE_HOME"/vim/{undo,swap,backup,view}

call plug#begin()
Plug 'w0ng/vim-hybrid'                     " color scheme
"" Plug 'chrisbra/csv.vim'                    " csv file handling
"" Plug 'godlygeek/tabular'                   " align stuff vertically
"" Plug 'airblade/vim-gitgutter'              " git diff
Plug 'jpalardy/vim-slime'                  " send commands to tmux pane
"" Plug 'tommcdo/vim-exchange'                " exchange text
Plug 'majutsushi/tagbar'                   " tag browser
"" Plug 'lisongmin/markdown2ctags'            " required by tagbar
"" Plug 'easymotion/vim-easymotion'           " better movement
"" Plug 'andymass/vim-matchup'                " even better matching
"" Plug 'ddffnn/vim-pandoc-syntax'            " better markdown syntax
"" Plug 'prabirshrestha/async.vim'            " \
"" Plug 'prabirshrestha/asyncomplete.vim'     "  \
"" Plug 'prabirshrestha/asyncomplete-lsp.vim' "   ) https://langserver.org/
"" Plug 'prabirshrestha/vim-lsp'              "  /
"" Plug 'mattn/vim-lsp-settings'              " /
Plug 'liuchengxu/vim-which-key'            " display available keybindings
"" Plug 'folke/which-key.nvim'
"" Plug 'cespare/vim-toml'                    " toml syntax support
Plug 'jeetsukumaran/vim-indentwise'        " motions based on identation levels
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

" https://stackoverflow.com/q/446269
noremap <space> <nop>
noremap <space> <leader>
let mapleader = "\<space>"
 
set number
set signcolumn=yes
set updatetime=300

set scrolloff=3
 
set linebreak
set breakindent
let &showbreak = '↳ '
 
set splitbelow
set splitright

set notimeout
set ttimeout
set ttimeoutlen=0

set smartindent

set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

set mouse=

set cursorline
set cursorcolumn

" turn off highligthing easily after searching
nnoremap <silent> <esc> :nohlsearch<return><esc>
set ignorecase
set smartcase
 
" true color support in terminal
" :help xterm-true-color
set termguicolors
set t_8f=[38;2;%lu;%lu;%lum
set t_8b=[48;2;%lu;%lu;%lum
colorscheme hybrid
 
"" highlight clear MatchParen
"" highlight MatchParen cterm=underline gui=underline
"" 
"" " change cursor based on mode
"" " https://stackoverflow.com/a/42118416
"" " but not supported by st: https://unix.stackexchange.com/a/486136
"" let &t_SI = "\e[6 q"
"" let &t_EI = "\e[2 q"

set undofile

"" set clipboard=""
"" 
"" ## set laststatus=2
"" set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]

" no crutches
noremap  <up>              <nop>
noremap  <down>            <nop>
noremap  <left>            <nop>
noremap  <right>           <nop>
inoremap <up>              <nop>
inoremap <down>            <nop>
inoremap <left>            <nop>
inoremap <right>           <nop>

"" " fix issue where vim starts in REPLACE mode
"" noremap  <esc>A            <nop>
"" 
"" " make Y behave like C and D
"" nnoremap Y y$
"" 
"" " add leader-based mappings for less chording
"" nmap <Leader>a <C-a>
"" nmap <Leader>x <C-x>
"" nmap <Leader>b <C-b>
"" nmap <Leader>f <C-f>
"" nmap <Leader>r <C-r>
"" 
"" " http://vim.wikia.com/wiki/VimTip47
"" nnoremap <silent> gw "_yiw:s/\(\%#\w\+\)\(\W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>:nohlsearch<CR>
"" nnoremap <silent> gl "_yiw?\w\+\_W\+\%#<CR>:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>:nohlsearch<CR>
"" nnoremap <silent> gr "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o>/\w\+\_W\+<CR><c-l>:nohlsearch<CR>
"" 
"" " make it easier to switch buffer
"" nnoremap gb :bnext<cr>
"" nnoremap gB :bprev<cr>
"" 
"" " disable ex mode
"" nnoremap Q <nop>
"" nnoremap gQ <nop>
"" 
"" " search and replace by visual selection
"" vnoremap <Leader>R "ky:%s/\<<C-r>k\>//gc<left><left><left>
"" 
"" " column alignment
"" " https://unix.stackexchange.com/a/179319
"" command! WhiteSpaceAlign execute ":'{+1,'}-1!column -t -s ' ' | sed -E 's/( *) /\\1/g'"

" https://vim.fandom.com/wiki/Make_views_automatic
set viewoptions=cursor,folds
augroup vimrc
    autocmd BufWritePost *
    \   if expand('%') != '' && &buftype !~ 'nofile'
    \|      mkview
    \|  endif
    autocmd BufRead *
    \   if expand('%') != '' && &buftype !~ 'nofile'
    \|      silent! loadview
    \|  endif
augroup END

autocmd FileType md,markdown,rmd setlocal shiftwidth=2 softtabstop=2 expandtab

"" " based on https://vi.stackexchange.com/a/2848
"" function! OneSentencePerLine(start, end)
""     silent execute a:start.','.a:end.'s/\n/ /g'
""     silent execute 's/  / /g'
""     silent execute 's/[.!?]\zs /\r/g'
"" endfunction
"" autocmd Filetype latex,tex,md,markdown,rmd,pandoc setlocal formatexpr=OneSentencePerLine(v:lnum,v:lnum+v:count-1)
"" 
"" " spell checking for certain file types
"" autocmd FileType latex,tex,md,markdown,rmd,pandoc setlocal spell
"" 
"" autocmd! BufNewFile,BufFilePre,BufRead *.Rmd set filetype=markdown.pandoc
"" 
"" " fix markdown autoindent insanity
"" " culprit was ..../indent/rmd.vim in vim distribution files
"" " https://stackoverflow.com/a/7331187
"" autocmd filetype rmd,md,markdown set indentexpr&
"" 
"" " plugin vim-pandoc-syntax
"" let g:pandoc#syntax#conceal#use = 0
"" let g:pandoc#syntax#codeblocks#embeds#use = 1
"" let g:pandoc#syntax#codeblocks#embeds#langs = ["bash=sh", "python", "fortran", "zsh", "r"]
"" 
"" " plugin easymotion
"" let g:EasyMotion_do_mapping = 0
"" let g:EasyMotion_startofline = 0
"" let g:EasyMotion_keys = 'euoia.p,ysnht'
"" let g:EasyMotion_inc_highlight = 1
"" let g:EasyMotion_smartcase = 1
"" map <leader>mj <Plug>(easymotion-j)
"" map <leader>mk <Plug>(easymotion-k)
"" map <leader>mw <Plug>(easymotion-wl)
"" map <leader>mW <Plug>(easymotion-W)
"" map <leader>mb <Plug>(easymotion-bl)
"" map <leader>me <Plug>(easymotion-el)
"" map <leader>mge <Plug>(easymotion-gel)
"" map <leader>mgE <Plug>(easymotion-gE)
"" map <leader>mt <Plug>(easymotion-tln)
"" map <leader>mT <Plug>(easymotion-Tln)
"" map <leader>mf <Plug>(easymotion-fln)
"" map <leader>mF <Plug>(easymotion-Fln)
"" "map <leader>mst <Plug>(easymotion-tl)<space>
"" "map <leader>msf <Plug>(easymotion-fl)<space>
"" "map <leader>msT <Plug>(easymotion-Tl)<space>
"" "map <leader>msF <Plug>(easymotion-Fl)<space>
"" 
"" " custom E motion that _is_ inclusive for deletion to end of word
"" " written by me using easymotion.vim as guide (plugin source code)
"" function! Easymotion_E() abort
""     call EasyMotion#User('\v\S(\s|$)|%$', 0, 0, 1)
"" endfunction
"" map <leader>mE :<c-u>call Easymotion_E()<CR>
"" 
"" " plugin tagbar
"" highlight clear TagbarHighlight
"" highlight TagbarHighlight ctermbg=bg cterm=underline guibg=bg gui=underline
nnoremap <leader>tb :TagbarToggle<CR>

" plugin slime
let g:slime_target = "tmux"
let g:slime_paste_file = $XDG_CACHE_HOME . "/vim/slime-paste"
let g:slime_no_mappings = 1
let g:slime_default_config = {"socket_name": "default", "target_pane": "1"}
nmap <leader>s  <Plug>SlimeMotionSend
nmap <leader>ss <Plug>SlimeLineSend
xmap <leader>s  <Plug>SlimeRegionSend

"" " plugin csv.vim
"" let g:csv_autocmd_arrange = 1
"" let g:csv_autocmd_arrange_size = 1024*1024
"" hi link CSVColumnOdd Keyword
"" hi link CSVColumnEven Macro
"" 
"" " fortran stuff
"" let fortran_do_enddo=1
"" let fortran_fold=1
"" 
"" " templates
"" if exists("$HOME/notes/templates")
""     autocmd BufNewFile Makefile 0r ~/notes/templates/Makefile
"" endif
"" 
"" " vim-lsp tab completion bindings
"" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"
"" 
"" let g:lsp_diagnostics_enabled = 1 
"" let g:lsp_signs_enabled = 1
"" let g:lsp_diagnostics_echo_cursor = 1

" coc
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" plugin vim-which-key
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler

let g:which_key_use_floating_win = 0
 
 nnoremap <silent> <leader>l :<c-u>WhichKey! g:which_key_lsp_dict<CR>
 let g:which_key_lsp_dict = {
             \ 'name' : 'lsp',           
             \ 'd' : ['<Plug>(coc-definition)',                 'definition'],
             \ 'e' : ['<Plug>(coc-diagnostic-next-error)',      'error'],
             \ 'E' : [':CocDiagnostics',                        'diagnostics'],
             \ 'h' : [":call CocAction('doHover')",             'hover'],
             \ 'H' : [':CocCommand document.toggleInlayHint',   'hints'], 
             \ 'r' : ['<Plug>(coc-references)',                 'references'],
             \ 'R' : ['<Plug>(coc-rename)',                     'rename'],
             \ }

"           \ 'D' : ['LspDeclaration',   'declaration'],
"           \ 's' : ['LspSignatureHelp', 'signature'],
"           \ 'w' : ['LspNextWarning',   'warning'],

" no worky with normal commands d, c, etc
" noremap <silent> <leader>m :<c-u>WhichKey! g:which_key_easymotion_dict<CR>
" let g:which_key_easymotion_dict = {
"             \ 'name' : 'easymotion',
"             \ 'j'    : ['<Plug>(easymotion-j)',   'down'],
"             \ 'k'    : ['<Plug>(easymotion-k)',   'up'],
"             \ 'w'    : ['<Plug>(easymotion-wl)' , 'word'],
"             \ 'W'    : ['<plug>(easymotion-W)',   'Word'],
"             \ 'b'    : ['<Plug>(easymotion-bl)',  'back'],
"             \ 'e'    : ['<Plug>(easymotion-el)',  'end'],
"             \ 'E'    : ['Easymotion_E()',         'End'],
"             \ 't'    : ['<Plug>(easymotion-tln)', 'til'],
"             \ 'f'    : ['<Plug>(easymotion-fln)', 'find'],
"             \ 'T'    : ['<Plug>(easymotion-Tln)', 'Til'],
"             \ 'F'    : ['<Plug>(easymotion-Fln)', 'Find'],
"             \ 's'    : {
"             \  'name': 'space',
"             \  't'   : ['<Plug>(easymotion-tl)\<space>', 'til space'],
"             \  'f'   : ['<Plug>(easymotion-fl)\<space>', 'find space'],
"             \  'T'   : ['<Plug>(easymotion-Tl)\<space>', 'Til space'],
"             \  'F'   : ['<Plug>(easymotion-Fl)\<space>', 'Find space'],
"             \ },
"             \ }

nnoremap <silent> <leader>i :<c-u>WhichKey! g:which_key_insert_dict<CR>
let g:which_key_insert_dict = {
            \ 'name' : 'insert',           
            \ 'd' : [":execute \"put=strftime('%Y-%m-%d')\"",    'date'],
            \ }
