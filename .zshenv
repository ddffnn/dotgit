if [ -z $XDG_CONFIG_HOME ]; then
    export XDG_CONFIG_HOME=$HOME/.config
fi
if [ -d $XDG_CONFIG_HOME/zsh ]; then
    export ZDOTDIR=$XDG_CONFIG_HOME/zsh
fi
if [ ! -z $XDG_DATA_HOME ]; then
    mkdir -p $XDG_DATA_HOME/zsh
fi
# get path to currently running shell (assumed to be zsh)
# https://unix.stackexchange.com/a/87065
export SHELL=`print -r -- /proc/self/exe(:A)`
