# Path Notes

Normally I use the Linux path when adding config files (that are used under Linux and Windows) to git.

However, this directory is checked into git under `~/AppData/Roaming/Mozilla/`.
This is because Windows Firefox won't apply `userContent.css` if the file is linked.

Linux Firefox does work with a linked `userContent.css`:

`ln -s ~/AppData/Roaming/Mozilla/Firefox/profiles/ddffnn.default/chrome ~/.mozilla/firefox/ddffnn.default/`

If [this bug](https://bugzilla.mozilla.org/show_bug.cgi?id=682706) is ever closed I might be able move this directory:
